# rtr-archive-keyring/Makefile

conf_list := $(patsubst \
	%.envsubst, \
	%, \
	$(sort $(wildcard *.conf.envsubst)) \
)
sources_list := $(sort \
	$(patsubst %.envsubst,%,$(wildcard *.sources.envsubst)) \
	$(patsubst %.gen.sh,%,$(wildcard *.sources.gen.sh)) \
)
keyring_list := $(patsubst \
	%.d, \
	%, \
	$(sort $(wildcard *-archive-keyring.gpg.d)) \
)
shell_scripts := \
  README.md.sh \
  debian/tests/acquire-retries \
  debian/tests/hello \
  debian/tests/ignore-files-silently \
  debian/tests/intel-opencl-icd-version \
  debian/tests/librealsense2-dev-version \
  debian/tests/nodejs-version \
  debian/tests/symlinks \
  generate-control \
  generate-installs \
  jetring-add \
  jetring-create \
  node.sources.gen.sh

# First target is the default.
.PHONY: all
all: $(conf_list) $(sources_list) $(keyring_list)

$(keyring_list): %.gpg: %.gpg.d/index
	jetring-build -I $@ $(dir $<)
	{ true \
	&& export GNUPGHOME="$$(mktemp -dt $@.XXXXXX)" \
	&& gpg \
		--import \
		--import-options=show-only \
		--no-auto-check-trustdb \
		--no-default-keyring \
		--no-keyring \
		--no-options \
		--with-colons \
		$@ \
	&& rm -rf "$${GNUPGHOME}" \
	; }

# debian, ubuntu
OS_ID ?= $(shell . /etc/os-release && echo "$${ID}")
# debian: stretch, buster, bullseye
# ubuntu: bionic, focal, jammy
OS_VERSION_CODENAME ?= $(shell . /etc/os-release && echo "$${VERSION_CODENAME}")
# debian: 9, 10, 11
# ubuntu: 18.04, 20.04, 22.04
OS_VERSION_ID ?= $(shell . /etc/os-release && echo "$${VERSION_ID}")
export OS_ID
export OS_VERSION_CODENAME
export OS_VERSION_ID

# available for focal:
#
# $ apt-cache policy intel-opencl-icd
# intel-opencl-icd:
#   Installed: 21.36.20889-1~ppa1~focal1
#   Candidate: 21.36.20889-1~ppa1~focal1
#   Version table:
#  *** 21.36.20889-1~ppa1~focal1 500
#         500 http://ppa.launchpad.net/intel-opencl/intel-opencl/ubuntu focal/main amd64 Packages
#         100 /var/lib/dpkg/status
#      20.13.16352-1 500
#         500 http://archive.ubuntu.com/ubuntu focal/universe amd64 Packages
#         500 http://security.ubuntu.com/ubuntu focal/universe amd64 Packages
#
# available for jammy:
#
# $ apt-cache policy intel-opencl-icd
# intel-opencl-icd:
#   Installed: (none)
#   Candidate: 22.14.22890-1
#   Version table:
#      22.14.22890-1 500
#         500 http://archive.ubuntu.com/ubuntu jammy/universe amd64 Packages
#
# If intel-opencl.sources is the target and focal is NOT the VERSION_CODENAME,
# then generate an empty file target; else, substitute environment variables as
# normal. We can do this because apt ignores empty 'sources.list' files.
#
# TODO: get rid of this hack when dropping support for focal and/or
# intel-opencl.
intel-opencl.sources: intel-opencl.sources.envsubst
	( set -euvx ; \
	if [ "focal" != "$${OS_VERSION_CODENAME}" ]; then \
		truncate -s0 $@ ; \
	else \
		envsubst '$${OS_ID},$${OS_VERSION_CODENAME},$${OS_VERSION_ID}' <$< >$@ ; \
	fi ; \
	)

%: %.envsubst
	envsubst '$${OS_ID},$${OS_VERSION_CODENAME},$${OS_VERSION_ID}' <$< >$@

%: %.gen.sh
	./$< >$@

.PHONY: check
check: shellcheck shfmt

# https://www.shellcheck.net/wiki/SC2317 : Command appears to be
# unreachable. Check usage (or ignore if invoked indirectly).
#
# https://www.shellcheck.net/wiki/SC2320 : This $? refers to echo/printf, not a
# previous command. Assign to variable to avoid it being overwritten.
#
.PHONY: shellcheck
shellcheck: $(shell_scripts)
	shellcheck --external-sources $^

.PHONY: shfmt
shfmt: $(shell_scripts)
	shfmt -i 4 -ci -bn -s -d -w -ln posix $^

# This Makefile creates and installs '*.sources' files.
#
# The 'debian/rules' file 'override_dh_install' target renames each '*.sources'
# file to a '*.sources.rtr' file.
.PHONY: install
install: $(conf_list) $(sources_list) $(keyring_list)
	mkdir -vp $(DESTDIR)/etc/apt/apt.conf.d/
	cp -va $(conf_list) $(DESTDIR)/etc/apt/apt.conf.d/
	mkdir -vp $(DESTDIR)/etc/apt/sources.list.d/
	cp -va $(sources_list) $(DESTDIR)/etc/apt/sources.list.d/
	mkdir -vp $(DESTDIR)/usr/share/keyrings/
	cp -va $(keyring_list) $(DESTDIR)/usr/share/keyrings/

.PHONY: clean
clean:
	rm -vf $(sort \
		$(conf_list) \
		$(keyring_list) \
		$(patsubst %,%.lastchangeset,$(keyring_list)) \
		$(sources_list) \
		$(wildcard *~) \
	)
