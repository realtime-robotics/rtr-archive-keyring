#!/bin/sh

# rtr-archive-keyring/node.sources.gen.sh

# This is the generator script for legacy 'node.sources'.

set -euvx

version_lt() {
    [ "$1" != "$2" ] && printf '%s\n%s\n' "$1" "$2" | sort -VC
}

################################################################################

OS_ID="${OS_ID:-$(. /etc/os-release && echo "${ID}")}"
OS_VERSION_CODENAME="${OS_VERSION_CODENAME:-$(. /etc/os-release && echo "${VERSION_CODENAME}")}"
OS_VERSION_ID="${OS_VERSION_ID:-$(. /etc/os-release && echo "${VERSION_ID}")}"
export OS_ID
export OS_VERSION_CODENAME
export OS_VERSION_ID

cat <<EOF
# /etc/apt/sources.list.d/node.sources

# NodeSource Node.js Binary Distributions

# https://github.com/nodesource/distributions

Types: deb deb-src
URIs: 
 http://deb.nodesource.com/node_14.x
 http://deb.nodesource.com/node_16.x
Suites: ${OS_VERSION_CODENAME}
Components: main
Signed-By: /usr/share/keyrings/node-archive-keyring.gpg
EOF
