rtr-archive-keyring (2024.12.27) unstable; urgency=medium

  * [1277456] Update file nodesource.sources.envsubst

 -- Chris Smith <csmith@rtr.ai>  Fri, 27 Dec 2024 13:06:35 -0500

rtr-archive-keyring (2024.03.04) unstable; urgency=medium

  [ Luke Drong ]
  * [b97f78b] Fix missed entry in rtr-archive-all

 -- Tom Kugelman <tom@tom-rtr-box>  Mon, 04 Mar 2024 11:31:24 -0500

rtr-archive-keyring (2024.02.28) unstable; urgency=medium

  [ Luke Drong ]
  * [a38026c] Add support for NodeSource changes to distribution following Fall 2023

 -- Tom Kugelman <tom@tom-rtr-box>  Wed, 28 Feb 2024 14:48:06 -0500

rtr-archive-keyring (2023.07.12) unstable; urgency=medium

  * [d8f4157] teach Makefile how to check:
  * [d41dced] shellcheck fixes
  * [997da19] update debian/control
  * [caad998] edit rtr.sources.envsubst: add ortools
  * [0d6b92d] add debian/tests/ortools-available

 -- Neil Roza <neil@rtr.ai>  Wed, 12 Jul 2023 23:03:30 +0000

rtr-archive-keyring (2023.04.04) unstable; urgency=medium

  * [e1d3e6a310] add debian/tests/nodejs-version; edit debian/tests/control
  * [0ea3dfa84a] Remove nodejs 16 for Ubuntu Focal.
    For Ubuntu Focal, remove access to the nodejs version 18 apt repository. Add an
    autopkgtest unit to verify as much.
    * edit Makefile:
      * assign sources_list variable from both the *.sources.envsubst
        *.sources.gen.sh files.
      * add pattern rule for generator scripts
    * remove node.sources.envsubst
    * create node.sources.gen.sh, a script that generates node.sources.

 -- Neil Roza <neil@rtr.ai>  Tue, 04 Apr 2023 21:06:07 +0000

rtr-archive-keyring (2023.03.16) unstable; urgency=medium

  * [78ad7e3fc9] update Standards-Version: 4.6.2
  * [cb7c17cd22] add debian/rtr-archive-all.docs: README.md
  * [5a8514888b] update *.sources.envsubst: add comments
  * [20a62ff630] update README.md
  * [a551d622ea] add node 18

 -- Neil Roza <neil@rtr.ai>  Thu, 16 Mar 2023 23:08:20 +0000

rtr-archive-keyring (2023.01.30) unstable; urgency=medium

  * [231ecf037b] update debian/gbp.conf
  * [1398f52612] RAPID-30366: rtr-archive-apt-config: let apt retry 10 times.
    Add a new apt.conf file that sets Acquire::Retries=10. Do this because it's
    easy to do and helps work around apt fetch failures caused by transient network
    access issues.
    * Add the apt.conf envsubst template file 99acquire-retries.conf.envsubst.
    * Modify debian/rtr-archive-apt-config.install via the generate-installs
      script.
    * Add debian/tests/acquire-retries.
    * Modify debian/tests/control to add debian/tests/acquire-retries.
    * Modify generate-installs: teach it how to install all *.conf.envsubst files
      with the binary package rtr-archive-apt-config.

 -- Neil Roza <neil@rtr.ai>  Mon, 30 Jan 2023 23:33:37 +0000

rtr-archive-keyring (2022.12.19) unstable; urgency=medium

  * [e686ad751b] Revert "remove realsense"
    This reverts commit fc79db95e36a5743af55811a59b8442ed81d72b0.
  * [d3cc454c86] mv realsense.sources.in realsense.sources.envsubst; sed s/VERSION_CODENAME/OS_VERSION_CODENAME/
  * [546294868f] add debian/tests/librealsense2-dev-version

 -- Neil Roza <neil@rtr.ai>  Mon, 19 Dec 2022 21:37:30 +0000

rtr-archive-keyring (2022.12.02) unstable; urgency=medium

  * [308b05facf] edit jetring-create: so much better
  * [3ad14f7363] edit Makefile: add OS_ID OS_VERSION_ID, VERSION_CODENAME -> OS_VERSION_CODENAME
  * [5097661a28] sed -i 's/[$][{]VERSION_CODENAME[}]/${OS_VERSION_CODENAME}/g' *.sources.envsubst

 -- Neil Roza <neil@rtr.ai>  Fri, 02 Dec 2022 23:22:12 +0000

rtr-archive-keyring (2022.11.21) unstable; urgency=medium

  * [885c41e618] edit rtr.sources.envsubst: add ppa:realtime-robotics/ubuntu/oldold
  * [d87b1727af] edit debian/tests/hello: vet indextargets harder
    (cherry picked from commit 16f49361500237675d5c620a175254ba2310bb0d)
  * [83b79f5189] edit debian/tests/hello: skip empty sources, better error diagnostics

 -- Neil Roza <neil@rtr.ai>  Mon, 21 Nov 2022 23:03:22 +0000

rtr-archive-keyring (2022.11.15) unstable; urgency=medium

  * Revert "add debian/tests/docker"
    This reverts commit 09d0bded1b701fad1bbe4462696a32f09e775456.
  * Revert "add docker"
    This reverts commit 1a44fd2e7eb6e89dd5d60cd824f9e6230496181f.

 -- Neil Roza <neil@rtr.ai>  Tue, 15 Nov 2022 00:48:22 +0000

rtr-archive-keyring (2022.11.14) unstable; urgency=medium

  * add docker
  * add debian/tests/docker

 -- Neil Roza <neil@rtr.ai>  Mon, 14 Nov 2022 20:50:36 +0000

rtr-archive-keyring (2022.11.13) unstable; urgency=medium

  * Tidy up. ==
    * edit `.gitignore`: add paths for `dpkg-buildpackage` cruft
    * edit `Makefile`:
      * `*.in` files are out. `*.envsubst` files are in.
      * add default (the first) target `all`, depends on `build`
      * specific rule for `intel-opencl.sources: intel-opencl.sources.envsubst`
      * generic rule for `%: %.envsubst`
      * Add comment about how the `Makefile` `install` plays with the
        `debian/rules` `override_dh_install`.
    * edit `debian/control`:
      * update `Standards-Version`
      * add `Homepage`
    * edit `debian/rules`: Add comment about how the `Makefile` `install` plays
      with the `debian/rules` `override_dh_install`.
    * edit `generate-control`:
      * Add lots of comments about what's happening and why.
      * Redirect stdout to `debian/control`.
      * Isolate slug enumerations in functions.
      * Call `wrap-and-sort -abst` at the end.
    * edit `generate-installs`:
      * Add so many comments.
    * Rename each `*.sources.in` file to a `*.sources.envsubst` file.

 -- Neil Roza <neil@rtr.ai>  Sun, 13 Nov 2022 17:48:41 +0000

rtr-archive-keyring (2022.08.19) unstable; urgency=medium

  * Revert "rm intel-opencl"
    This reverts commit fe669547f5a706ee0f6a7dba07ec9f4d03fc38c2.
  * edit Makefile: intel-opencl.sources non-empty only on focal
  * update debian/tests/symlinks: apt-cache depends rtr-archive-all
  * edit debian/tests/hello: rm spit_vars, apt-cache depends rtr-archive-all
  * edit debian/tests/ignore-files-silently: rm spit_vars
  * edit debian/tests/hello: whitespace
  * edit debian/tests/symlinks: rm spit_vars
  * add debian/tests/intel-opencl-icd-version

 -- Neil Roza <neil@rtr.ai>  Fri, 19 Aug 2022 23:35:41 +0000

rtr-archive-keyring (2022.08.16) unstable; urgency=medium

  [ howieChiang ]
  * Update changelog for 2022.07.20 release

 -- howie <howie@rtr.ai>  Tue, 16 Aug 2022 11:08:33 -0400

rtr-archive-keyring (2022.07.20) unstable; urgency=medium

  [ Neil Roza ]
  * edit node.sources: rm v10, v12
  * add debian/gbp.conf
  * debhelper-compat 13
  * edit ros.sources: rm ros (keep ros2)
  * rm intel-opencl

 -- Neil Roza <neil@rtr.ai>  Wed, 20 Jul 2022 19:45:51 +0000

rtr-archive-keyring (2022.02.07) unstable; urgency=medium

  * remove realsense
  * update README.md

 -- Neil Roza <neil@rtr.ai>  Mon, 07 Feb 2022 18:44:26 +0000

rtr-archive-keyring (2022.01.31) unstable; urgency=medium

  * edit rtr.sources.in: add orocos-kdl
  * wrap-and-sort -asbt

 -- Neil Roza <neil@rtr.ai>  Mon, 31 Jan 2022 21:05:40 +0000

rtr-archive-keyring (2022.01.26) unstable; urgency=medium

  [ howieChiang ]
  * add trac-ik

 -- howie <howie@rtr.ai>  Wed, 26 Jan 2022 11:26:41 -0500

rtr-archive-keyring (2022.01.11) unstable; urgency=medium

  * edit rtr.sources.in: add debug symbols

 -- Neil Roza <neil@rtr.ai>  Tue, 11 Jan 2022 00:04:27 +0000

rtr-archive-keyring (2022.01.06) unstable; urgency=medium

  * edit node.sources.in: add v16

 -- Neil Roza <neil@rtr.ai>  Thu, 06 Jan 2022 18:56:25 +0000

rtr-archive-keyring (2021.12.14) unstable; urgency=medium

  * RAPID-11891: tell apt to ignore *.rtr extensions
    add 00ignore-rtr.conf.in and assorted candy to make it work

 -- Neil Roza <neil@rtr.ai>  Tue, 14 Dec 2021 21:06:39 +0000

rtr-archive-keyring (2021.11.30) unstable; urgency=medium

  * edit Makefile
    * populate keyring_list from *-archive-keyring.gpg.d
    * foreach jetring-build keyring, verbose dump with gpg --with-colons
  * edit Makefile: export GNUPGHOME for diagnostics

 -- Neil Roza <neil@rtr.ai>  Tue, 30 Nov 2021 16:17:36 +0000

rtr-archive-keyring (2021.11.29) unstable; urgency=medium

  * edit jetring-create: add script boilerplate
  * add jetring-add
  * ./jetring-add rtr-archive-keyring.gpg.d 0x423d522aa23b50c66f214f97219eacce6579994c
  * fix path comments
  * edit generate-control
    * add scripting boilerplate
    * change longdescify to kill line-trailing whitespace
    * generate the Source stanza
    * foreach $SLUG-archive-keyring.gpg.d directory, generate Package
      $SLUG-archive-keyring
    * foreach $SLUG.sources.in file, generate Package $SLUG-archive-apt-config;
      give ubuntu the special case it needs
    * foreach Package, add 'Multi-Arch: allowed'
    * add 'rtr-archive-all' metapackage
  * update debian/control
  * edit debian/tests/hello: spit more diagnostics

 -- Neil Roza <neil@rtr.ai>  Mon, 29 Nov 2021 22:45:21 +0000

rtr-archive-keyring (2021.11.24) unstable; urgency=medium

  * fix broken symlinks
    * debian/tests/symlinks
    * override displace-extension
    * edit generate-installs to generate debian/*.displace-extension
  * edit rtr.sources.in: add ppa:realtime-robotics/keyrings
  * edit rtr.sources.in: add ppa:realtime-robotics/old
  * edit rtr.sources.in: add ppa:realtime-robotics/phoxi

 -- Neil Roza <neil@rtr.ai>  Wed, 24 Nov 2021 03:48:02 +0000

rtr-archive-keyring (2021.11.23) unstable; urgency=medium

  * start using config-package-dev
  * generate installs and displaces
  * edit debian/rules: override_dh_install for config-package extension
  * add debian/*.install debian/*.displace
  * edit debian/control: +Provides, +Conflicts, tweak ubuntu
  * edit generate-control: +Provides, +Conflicts, comment each stanza
  * add autopkgtest stuff

 -- Neil Roza <neil@rtr.ai>  Tue, 23 Nov 2021 16:58:57 +0000

rtr-archive-keyring (2021.11.22) unstable; urgency=medium

  * Initial release.

 -- Neil Roza <neil@rtr.ai>  Mon, 22 Nov 2021 20:23:18 +0000
